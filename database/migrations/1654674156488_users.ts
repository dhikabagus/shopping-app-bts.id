import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'users'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('username', 45).notNullable()
      table.string('password', 12).notNullable()
      table.string('email').notNullable()
      table.string('remember_me_token').nullable()
      table.string('phone')
      table.string('country')
      table.string('city')
      table.string('postcode')
      table.string('name')
      table.text('address')
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps(true,true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
