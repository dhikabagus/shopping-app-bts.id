import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import ShoppingValidator from 'App/Validators/ShoppingValidator'
import Shopping from 'App/Models/Shopping'

export default class ShoppingsController {

  public async index({ response }: HttpContextContract) {
    const shoppings = await Shopping.all()
    response.ok({ message: "Seluruh data berhasil didapatkan", data: shoppings })
  }

  public async show({ params, response }: HttpContextContract) {
    let id = params.id
    const shopping = await Shopping
      .query() // 👈now have access to all query builder methods
      .where('id', id)
      .orWhereNull('id')
      .firstOrFail()
    // const shopping = await Shopping.find(id)
    return response.status(200).json({ message: "Data berhasil didapatkan", data: shopping })
  }

  public async store({ request, response }: HttpContextContract) {
    try {
        
      const shopping = new Shopping()
      console.log(request.body())
      await shopping.fill(request.body()).save()
      return response.ok({ message: "Data berhasil ditambahkan", data: shopping })
    } catch (err) {
      return response.badRequest({ error: err.message })
    }
  }

  public async update({ params, request, response }: HttpContextContract) {
    try {
      const shopping = await Shopping.findOrFail(params.id)
      await request.validate(ShoppingValidator)
      let updatedData = request.body()
      await shopping
        .merge(updatedData)
        .save()
      return response.ok({ message: "Data berhasil diubah", data: request.body() })
    } catch (err) {
      return response.badRequest({ error: err.messages })
    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    try {
      let id = params.id
      const shopping = await Shopping.findOrFail(id)
      await shopping.delete()
      return response.ok({ message: "Data berhasil dihapus", data: id })
    } catch (err) {
      return response.badRequest({ error: err.messages })
    }
  }
}
