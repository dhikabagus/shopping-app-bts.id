import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import { schema } from '@ioc:Adonis/Core/Validator'

export default class AuthController {
    public async signUp({request, response }: HttpContextContract) {
        try {
            
            const newUser = new User()
            newUser.username = request.body().user.username
            newUser.email = request.body().user.email
            newUser.password = request.body().user.password
            newUser.phone = request.body().user.phone
            newUser.address = request.body().user.address
            newUser.city = request.body().user.city
            newUser.country = request.body().user.country
            newUser.name = request.body().user.name
            newUser.postcode = request.body().user.postcode

            // Insert to the database
            await newUser.save()

            // const token = await auth.use('api').attempt(newUser.email, newUser.password)

            return response.created({email: newUser.email,username:newUser.username})
        } catch (err) {
            return response.unprocessableEntity({ message: "Failed SignUp", error: err.message })
        }
    }

    public async signIn({auth,request,response}: HttpContextContract){
        const userSchema = schema.create({
            email: schema.string(),
            password: schema.string()
        })

        try {
            const user = await request.validate({ schema: userSchema })
            const { email, password } = request.body()
            const token = await auth.use('api').attempt(email, password)
            console.log(user)
            return response.ok({ message: "login success", token: token })
        } catch (err) {
            if (err.guard) {
                console.log(err.messsage)
                return response.badRequest({ message: 'login error', error: err.message })
            } else {
                console.log(err.messages)
                return response.badRequest({ message: 'login error', error: err.message })
            }
        }
    }

    public async getAllUser({response}: HttpContextContract){
        const users = await User.all()
        response.ok({ message: "Seluruh data berhasil didapatkan", data: users })
    }
}
