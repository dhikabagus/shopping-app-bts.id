/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

//auth
Route.get('getAllUser', 'AuthController.getAllUser').as('auth.getAllUser').prefix('api/')
Route.post('signUp', 'AuthController.signUp').as('auth.signUp').prefix('api/')
Route.post('signIn', 'AuthController.signIn').as('auth.signIn').prefix('api/')


//Shopping
Route.resource('api/shoppings', 'ShoppingsController').except(['create','edit'])
